/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

$(function() {
    loadAnimation('#instant-payments-animation', '/assets/js/about/overview/animation/instant_payments.json');
    loadAnimation('#data-privacy-animation', '/assets/js/about/overview/animation/data_privacy.json');
    loadAnimation('#decentralized-network-animation', '/assets/js/about/overview/animation/decentralized_network.json');
    loadAnimation('#scalable-network-animation', '/assets/js/about/overview/animation/scalable_network.json');
});