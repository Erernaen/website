/** 
 * ecc.network
 * 
 * @copyright Project ECC
 */

function loadAnimation(targetElSelector, jsonPath) {
    var $matchedEls = $(targetElSelector);
    if ($matchedEls.length > 0) {
        lottie.loadAnimation({
            container: $matchedEls[0],
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: jsonPath
        });
    } else {
        console.log('ERROR: Animation unable to load--el `' + targetElSelector + '` not found.');
    }
}

// GA config
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-115450514-1');

$(function() {
    /*
     * _header
     */
    // change header transparency and logo color on scroll
    var $window = $(window);
    var $body = $('body');
    var $header = $('header');
    if (!$body.hasClass('no-scroll')) {
        $window.on('scroll', function() {
            if ($window.scrollTop() > 50) {
                $header.addClass('scrolled');
            } else {
                $header.removeClass('scrolled');
            }
        });
    }

    var $menu = $('#navbar-menu');
    $menu.find('.dropdown').hover(function() {
        $(this).find('.dropdown-menu')
            .stop(true, true)
            .fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu')
            .stop(true, true)
            .fadeOut(500);
    });

    var $mobileNavToggle = $('#mobile-nav-toggle');
    $mobileNavToggle.click(function() {
        var $icon = $mobileNavToggle.find('.fa');
        if ($mobileNavToggle.hasClass('show')) {
            $icon.removeClass('fa-times')
                .addClass('fa-bars');
        } else {
            $icon.removeClass('fa-bars')
                .addClass('fa-times');
        }
        $mobileNavToggle.toggleClass('show');
        $('#mobile-navbar-menu').slideToggle();
    });

    $('.locale-link').click(function() {
        document.cookie = 'locale=' + $(this).data('locale') + '; path=/';
        location.reload();
    });

    /*
     * (Global)
     */
    var titleFadeIn = function() {
        $('.fade-in').animate({marginLeft: 0, opacity: 1}, 250, 'linear', function() {$window.off('scroll', titleFadeIn)});
    };
    // if user starts scrolling before 1 second, we're good
    $window.on('scroll', titleFadeIn);
    // they didn't--ease the title in for them
    setTimeout(titleFadeIn, 750);

    // set up modal on video btns
    $('[data-toggle="video"]').modalVideo();

    // set up tooltips
    $('[data-toggle="tooltip"]').tooltip();

    /*
     * _footer
     */
    $('.social-icons')
        .find('img')
        .hover(
            function() {var $me = $(this); $me.attr('src', $me.attr('src').replace(/.svg/, '-hover.svg'));},
            function() {var $me = $(this); $me.attr('src', $me.attr('src').replace(/-hover/, ''));}
        );
});